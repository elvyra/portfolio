// Darbinis objektų masyvas, kuriame saugomi įrašai
let cars = [];
// Lentelė, į kurią atvaizduojamas mašinų registras
let printCarsTable = document.getElementById('cars_table');
// Laukiamas naujo įrašo įkėlimo mygtuko paspaudimas
let btnNew = document.getElementById('btn_register_new');
btnNew.addEventListener('click', registerCar);

let btnClear = document.getElementById('btn_clear');
btnClear.addEventListener('click', clearLocalStorage);


// Laukiama, kol užsikrauna HTML dokumentas ir atspausdinamas localstorage saugomas turinys
document.addEventListener("DOMContentLoaded", function(event) {
    cars = getFromStorage();
    if (cars === null) {
        cars = [];
    }
    printCars(cars, printCarsTable);
  });


// Laukiama paspaudimų ant dinaminių JS sukurtų elementų (rikiavimas)
document.body.addEventListener( 'click', function ( event ) {
    if (event.srcElement.id == 'sort_Brand') { cars = sortBrand (cars); printCars(cars, printCarsTable);
        } else if (event.srcElement.id == 'sort_Model') {  cars = sortModel (cars); printCars(cars, printCarsTable);
        } else if (event.srcElement.id == 'sort_Engine') {  cars = sortEngine (cars); printCars(cars, printCarsTable);
    }
  } );

// Įrašų įkėlimas iš localStorage
function getFromStorage () {
    let carsArray = [];
    if (typeof(Storage) !== "undefined") {
        let text = localStorage.getItem("carArray");
        if (text === "") { return carsArray; }
        carsArray = JSON.parse(text);
        return carsArray;
    } else {
       return carsArray;
    }
}

// Masyvo pateikimas lentelėje
function printCars(carsArray, carsTable) {
    if (cars) {
        carsTable.innerHTML = `
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Brand <i id="sort_Brand" class="fas fa-sort"></i></th>
                                <th>Model <i id="sort_Model" class="fas fa-sort"></i></th>
                                <th>Engine <i id="sort_Engine" class="fas fa-sort"></i></th>
                                <th>Remove</th>
                            </tr>
                        </thead> `;
        carsTable.innerHTML += "<tbody>";
        carsArray.forEach(function(car, index) {
            carsTable.innerHTML += `
                <tr>
                    <td>${index+1}</td>
                    <td>${car.Brand}</td>
                    <td>${car.Model}</td>
                    <td>${car.Engine}</td>
                    <td><a href="#" class="btn btn-danger btn-xs">X</a></td>
                </tr>
                `;
        });
        carsTable.innerHTML += `</tbody>`;
    } else { carsTable.innerHTML="None"; }
}

//Duomenų gavimas iš formos, gražina įrašo tipo elementą
function getFromForm () {
    let brand = document.getElementById('brand').value;
    let model = document.getElementById('model').value;
    let engine = document.getElementById('engine').value;

    if (!brand || !model || !engine) {
        alert('Fill in all text areas first.');
        return false;
    } 
    if (isNaN(engine)) {
        alert('Engine must be a valid number.');
        return false;
    } else { engine = Number(engine); }

    let car = {
        Brand: brand,
        Model: model,
        Engine: engine
    }
    return car;
}

//Naujo automobilio įregistravimas
function registerCar(){
    //Duomenų iš teksto laukelių nuskaitymas ir patalpinimas į masyvą bei localstorage
    cars.push(getFromForm());
    localStorage.setItem("carArray", JSON.stringify(cars));
    //Duomenų įvedimo formos reset
    document.getElementById("carForm").reset;
    //Duomenų atvaizdavimas lentelėje
    printCars(cars, printCarsTable);
}

function sortBrand (array) {
    array.sort(function(a, b) {
        var nameA = a.Brand.toUpperCase(); // ignore upper and lowercase
        var nameB = b.Brand.toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;        }      
        // names must be equal
        return 0;
    });
    return array;
}

function sortModel (array) {
    array.sort(function(a, b) {
        var nameA = a.Model.toUpperCase(); // ignore upper and lowercase
        var nameB = b.Model.toUpperCase(); // ignore upper and lowercase
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;        }      
        // names must be equal
        return 0;
    });
    return array;
}

function sortEngine (array) {
    array.sort(function (a, b) {
        return a.Engine - b.Engine;
    });
    return array;
}

function clearLocalStorage() {
    var desition = confirm("You are deleting all registered cars from memory. Data restore will be imposible.");
    if (desition == true) {
        localStorage.removeItem("carArray");
        cars = getFromStorage();
        printCars(cars, printCarsTable);
        alert("Data is deleted.")
    } else {
        return false;
    }
}